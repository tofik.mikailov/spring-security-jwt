package io.javabrains.springsecurityjwt.repository;

import io.javabrains.springsecurityjwt.models.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Integer> {

    Optional<User> findByUserName(String username);
}
