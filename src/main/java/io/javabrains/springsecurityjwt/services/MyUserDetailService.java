package io.javabrains.springsecurityjwt.services;

import io.javabrains.springsecurityjwt.models.MyUserDetails;
import io.javabrains.springsecurityjwt.models.User;
import io.javabrains.springsecurityjwt.repository.UserRepository;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUserName(username);

        user.orElseThrow(() -> new UsernameNotFoundException("Not found " + username));

       return user.map(MyUserDetails::new).get();
    }
}
